package testcheater.model;

public class DataInput {

  private String question;
  private String answer;

  public DataInput(String frage, String answer)
  {
    this.question = frage;
    this.answer = answer;
  }

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String frage) {
    this.question = frage;
  }

  public String getAnswer() {
    return answer;
  }

  public void setAnswer(String answer) {
    this.answer = answer;
  }

  @Override
  public String toString()
  {
    return "q: "+question +" a: "+ answer;
  }
}
