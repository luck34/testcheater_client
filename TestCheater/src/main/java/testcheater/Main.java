package testcheater;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.File;
import java.net.URL;
import java.nio.file.Paths;

public class Main extends Application {

  @Override
  public void start(Stage primaryStage) throws Exception {
    FileChooser inputFileChooser = new FileChooser();
    inputFileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("Frage/Antworten - Datei","*.test"));
    File f = inputFileChooser.showOpenDialog(primaryStage);

    if(f != null) {
      FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("MainFXML.fxml"));

      Group root = loader.load();

      Scene myScene = new Scene(root, Color.TRANSPARENT);

      Controller controller = loader.getController();
      controller.initialize(primaryStage, myScene);
      controller.initData(f);

      primaryStage.setAlwaysOnTop(true);
      primaryStage.setScene(myScene);
      primaryStage.initStyle(StageStyle.TRANSPARENT);
      primaryStage.show();
    }else
    {
      Alert alert = new Alert(Alert.AlertType.ERROR);
      alert.setHeaderText("You need to select a File!");
      alert.showAndWait();

      System.exit(0);
    }
  }


  public static void main(String[] args) {
    launch(args);
  }
}
