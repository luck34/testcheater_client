package testcheater.util;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import testcheater.model.DataInput;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class ReadData {

  private File source;

  public ReadData(File source)
  {
    this.source = source;
  }

  public List<DataInput> getData() throws IOException {
    LinkedList<DataInput> data = new LinkedList<>();

    Gson gson = new Gson();

    JsonArray rootArray = gson.fromJson(new FileReader(source), JsonArray.class);

    for(JsonElement element : rootArray)
    {
      JsonObject tmpObject = element.getAsJsonObject();

      String question = tmpObject.get("question").getAsString();
      String answer = tmpObject.get("answer").getAsString();

      data.add(new DataInput(question, answer));
    }

    return data;
  }
}
