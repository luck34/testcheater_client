package testcheater;

import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import testcheater.model.DataInput;
import testcheater.util.ReadData;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Controller {

  public Rectangle backRect;
  @FXML
  Rectangle rootRect;

  private double initDragX, initDragY;

  private ArrayList<DataInput> data;

  public void initialize(Stage parentStage, Scene parentScene)
  {
    // drag-window
    rootRect.setOnMousePressed(me -> {
      initDragX = me.getScreenX() - parentStage.getX();
      initDragY = me.getScreenY() - parentStage.getY();
    });
    rootRect.setOnMouseDragged(me -> {
      parentStage.setX(me.getScreenX() - initDragX);
      parentStage.setY(me.getScreenY() - initDragY);
    });

    // Border-Color
    Stop[] stops = new Stop[] { new Stop(0, Color.BLACK), new Stop(1, Color.WHITE)};
    LinearGradient lg1 = new LinearGradient(0, 0, 1, 1, true, CycleMethod.NO_CYCLE, stops);

    backRect.setStroke(lg1);

    rootRect.setOnMouseEntered(e -> {
      try {
        String partOfQuestion = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);

        for(DataInput i : data)
        {
          if(i.getQuestion().toLowerCase().contains(partOfQuestion.toLowerCase()))
          {
            StringSelection selection = new StringSelection(i.getAnswer());
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            clipboard.setContents(selection, selection);

            break;
          }
        }

      } catch (UnsupportedFlavorException e1) {
        e1.printStackTrace();
      } catch (IOException e1) {
        e1.printStackTrace();
      }
    });
  }

  public void initData(File f)
  {
    ReadData readData = new ReadData(f);
    try {
      data = new ArrayList(readData.getData());

    } catch (IOException e) {
      Alert alert = new Alert(Alert.AlertType.ERROR);
      alert.setHeaderText("Fileinput is of wrong type!");
      alert.showAndWait();

      System.exit(0);
    }
  }

}
