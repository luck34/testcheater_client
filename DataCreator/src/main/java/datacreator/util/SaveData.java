package datacreator.util;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import datacreator.model.DataInput;
import javafx.collections.ObservableList;

import javax.xml.crypto.Data;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SaveData {

  private File destination;
  private ObservableList<DataInput> data;

  public SaveData(File destination, ObservableList<DataInput> data)
  {
    this.destination = destination;

    this.data = data;
  }

  public void execute() throws IOException {
    PrintWriter writer = new PrintWriter(destination);

    Gson gson  = new Gson();

    JsonArray rootArray = new JsonArray();

    data.forEach(x -> {
      JsonObject tmp = new JsonObject();
      tmp.addProperty("question", x.getFrage());
      tmp.addProperty("answer", x.getAntwort());

      rootArray.add(tmp);
    });

    writer.println(rootArray.toString());

    writer.close();
  }

}
