package datacreator.util;

import com.google.gson.*;
import datacreator.model.DataInput;
import javafx.collections.ObservableList;

import java.io.*;
import java.util.ArrayList;

public class OpenData {

  private File destination;

  public OpenData(File destination)
  {
    this.destination = destination;
  }

  public ArrayList<DataInput> getData() throws IOException {
    ArrayList<DataInput> list = new ArrayList<>();

    BufferedReader reader = new BufferedReader(new FileReader(destination));

    StringBuilder text = new StringBuilder();
    String tmp;
    while((tmp = reader.readLine()) != null)
    {
      text.append(tmp);
    }

    JsonParser parser = new JsonParser();
    JsonElement root = parser.parse(text.toString());

    JsonArray array = root.getAsJsonArray();

    for(JsonElement el : array)
    {
      String ques = el.getAsJsonObject().get("question").getAsString();
      String ans = el.getAsJsonObject().get("answer").getAsString();
      DataInput di = new DataInput(ques, ans);
      list.add(di);
    }

    return list;
  }
}
