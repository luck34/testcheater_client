package datacreator;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

  @Override
  public void start(Stage primaryStage) throws IOException {
    FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("MainFXML.fxml"));

    BorderPane rootPane = loader.load();
    MainController controller = loader.getController();

    Scene myScene = new Scene(rootPane);

    controller.initialize(primaryStage, myScene);

    primaryStage.setScene(myScene);
    primaryStage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }

}
