package datacreator;

import datacreator.model.DataInput;
import datacreator.util.OpenData;
import datacreator.util.SaveData;
import datacreator.util.TextAreaTableCell;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class MainController {

  public Button openBtn;
  private Stage parentStage;
  private Scene parentScene;

  public TableView table;
  public Button addItemBtn;
  public Button saveBtn;

  public TableColumn questionColumn;
  public TableColumn answerColumn;

  private ObservableList<DataInput> myData = FXCollections.observableArrayList();

  public void initialize(Stage parentStage, Scene parentScene)
  {
    this.parentStage = parentStage;
    this.parentScene = parentScene;

    // make editable
    questionColumn.setCellFactory(TextAreaTableCell.forTableColumn());
    questionColumn.setOnEditCommit(
            new EventHandler<CellEditEvent<DataInput, String>>() {
              @Override
              public void handle(CellEditEvent<DataInput, String> t) {
                ((DataInput) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                ).setFrage(t.getNewValue());
              }
            }
    );

    answerColumn.setCellFactory(TextAreaTableCell.forTableColumn());
    answerColumn.setOnEditCommit(
            new EventHandler<CellEditEvent<DataInput, String>>() {
              @Override
              public void handle(CellEditEvent<DataInput, String> t) {
                ((DataInput) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                ).setAntwort(t.getNewValue());
              }
            }
    );

    table.setItems(myData);
  }

  public void onRemoveItem(ActionEvent event)
  {
    myData.remove(table.getSelectionModel().getSelectedItem());
  }
  public void onAddItem(ActionEvent event)
  {
    myData.add(new DataInput("[QUESTION]", "[ANSWER]"));
  }

  public void onSaveItems(ActionEvent event)
  {
    FileChooser saveFileChooser = new FileChooser();
    saveFileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("Frage/Antworten - Files", "*.test"));
    File selection = saveFileChooser.showSaveDialog(parentStage);

    if(selection != null)
    {
      SaveData saveData = new SaveData(selection, myData);
      try {
        saveData.execute();
        Alert infoMsg = new Alert(Alert.AlertType.INFORMATION);
        infoMsg.setHeaderText("Successful finished!");
        infoMsg.show();
      } catch (IOException e) {
        Alert errorMsg = new Alert(Alert.AlertType.ERROR);
        errorMsg.setTitle("Error");
        errorMsg.setContentText(e.getMessage());
        errorMsg.show();
      }
    }
  }

  public void onOpenItems(ActionEvent actionEvent) {
    FileChooser saveFileChooser = new FileChooser();
    saveFileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("Frage/Antworten - Files", "*.test"));
    File selection = saveFileChooser.showOpenDialog(parentStage);

    if(selection != null)
    {
      OpenData openData = new OpenData(selection);

      try {
        ArrayList<DataInput> list = openData.getData();

        myData.addAll(list);
      } catch (IOException e) {
        Alert fail = new Alert(Alert.AlertType.ERROR);
        fail.setContentText("error with file");
        fail.show();
      }
    }
  }
}
