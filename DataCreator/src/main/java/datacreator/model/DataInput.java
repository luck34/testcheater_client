package datacreator.model;

import javafx.beans.property.SimpleStringProperty;

public class DataInput {

  private SimpleStringProperty questionProperty;
  private SimpleStringProperty answerProperty;

  public DataInput(String frage, String answer)
  {
    this.questionProperty = new SimpleStringProperty(frage);
    this.answerProperty = new SimpleStringProperty(answer);
  }

  public String getFrage() {
    return questionProperty.get();
  }

  public void setFrage(String frage) {
    this.questionProperty.set(frage);
  }

  public String getAntwort() {
    return answerProperty.get();
  }

  public void setAntwort(String antwort) {
    this.answerProperty.set(antwort);
  }
}
